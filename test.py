from machine import SPI, I2C, Pin
from drivers import vs1003
from drivers import ssd1306
from drivers.font.symbol import HEART
from utime import sleep


def run_all_test():
	run_decoder_sine_test()
	run_decoder_mp3_test()
	run_oled_test()


def _init_player():
	spi = SPI(1)
	spi.init(
		sck=Pin(14),
		mosi=Pin(13),
		miso=Pin(12),
		baudrate=vs1003.BAUDRATE_COMMAND,
		polarity=0,
		phase=0
	)

	player = vs1003.Player(
		spi,
		xdcs=Pin(26, Pin.OUT, value=1),
		xcs=Pin(25, Pin.OUT, value=1),
		dreq=Pin(33, Pin.IN),
		rst=Pin(32, Pin.OUT, value=1)
	)

	player.set_volume(0.2)

	return player


player = _init_player()


def run_decoder_sine_test():
	player.play_test()

	print("version: ", player.version)

	# inputFile = open('test.mp3', mode='rb')
	# buf = bytearray(32)
	# while inputFile.readinto(buf):
	# 	player.writeData(buf)


def run_decoder_mp3_test(myPlayer):
	from media import music

	myPlayer.reset()

	for data in music.music2:
		myPlayer.play_chunk(bytes([data]))


def run_oled_test():
	i2c = I2C(-1, scl=Pin(5), sda=Pin(4))
	oled = ssd1306.SSD1306_I2C(128, 64, i2c)

	oled.text("Hello, world!!!", 1, 1)
	oled.show()
	sleep(3)

	oled.show_symbol(HEART, 118, 5)
	oled.show()
	sleep(3)

	oled.show_chinese("我是你爸爸", 24, 24, 10, 10)
	oled.show()
	sleep(5)

	oled.poweroff()
