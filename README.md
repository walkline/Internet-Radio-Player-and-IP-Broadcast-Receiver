# Internet Radio Player and IP Broadcast Receiver

之前为了玩游戏方便语音交流，所以撸了一个 [局域网语音通话软件](https://gitee.com/walkline/voice_chat)，这里用到了 `UDP 广播`，所以在考虑可不可以做一个类似 IP 广播的东西，数据发送端已经有了，那么就差一个接收端了，在网上找到了这篇文章 [Internet Radio Using an ESP32](https://www.instructables.com/id/Internet-Radio-Using-an-ESP32/)，经过仔细研究发现（虽然水平有限，但）应该可以实现吧？

> 第一次用 ESP32，还真有点小紧张哈哈哈哈哈哈

## 项目目标

* 实现网络收音机功能
* 实现 IP 广播接收播放功能
* 两项功能提供切换按钮
* 最好还能设计一个盒子去组装项目里用到的硬件。。。。

## 准备物料

没有购买链接，就放几张示意图吧

* **ESP32 开发板** * 1
* **MP3 解码器** (VS~~1053~~1003 模块，SPI 接口) * 1 ~~垃圾！骗子！！挂羊头卖狗肉！！！~~
* **数字功放** (PAM8403 模块，带开关电位器) * 1
* **OLED** (SSD1306 模块，0.96 英寸，128*64，IIC 接口) * 1
* **喇叭** (3瓦 / 4欧) * 1~2
* **自锁按钮** * 1
* **非自锁按钮** * 3
* **连接线** * 若干

## 工具软件

* [Dot Matrix Generator](https://gitee.com/walkline/Dot-Matrix-Generator) 用于提取字模的软件 
## 参考资料

* [Internet Radio Using an ESP32 (整体项目参考资料)](https://www.instructables.com/id/Internet-Radio-Using-an-ESP32/)
* [MicroPython_SSD1306 驱动](https://github.com/AnthonyKNorman/MicroPython_SSD1306)
* [micropython/drivers/display (SSD1306 官方驱动)](https://github.com/micropython/micropython/tree/master/drivers/display)
* [SSD1306 资料下载](https://gitee.com/walkline/Internet-Radio-Player-and-IP-Broadcast-Receiver/attach_files/251650/download)
* [VS1053 Driver for CircuitPython (VS1053 驱动)](https://github.com/urish/vs1053-circuitpython)
* [Adafruit_CircuitPython_VS1053 (VS1053 驱动)](https://github.com/adafruit/Adafruit_CircuitPython_VS1053)
* [VS1003 资料下载](https://gitee.com/walkline/Internet-Radio-Player-and-IP-Broadcast-Receiver/attach_files/251872/download)
* [真·VS1053 资料下载](https://gitee.com/walkline/Internet-Radio-Player-and-IP-Broadcast-Receiver/attach_files/252111/download)
