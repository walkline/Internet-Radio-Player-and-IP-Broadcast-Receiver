def debug_info(msg):
	"""
	print message if in DEBUG mode
	"""

	from config import Config

	if Config.DEBUG:
		if isinstance(msg, str):
			print(msg)
		else:
			print(str(msg, "utf-8"))


def gateway_to_udp_host(gateway: str):
	return None if gateway is None else gateway[:-1] + '255'


def is_info_file_exist():
	from config import Config
	import os

	try:
		os.stat(Config.INFORMATION_FILE)

		return True
	except OSError:
		return False


def delete_info_file():
	import os
	from config import Config

	try:
		os.remove(Config.INFORMATION_FILE)
	except OSError:
		pass


def get_information():
	"""
	read information from file
	:return: essid & password if success else False
	"""

	"""
	if type(get_information()) is not bool:
		essid, password = 
	"""

	from config import Config

	try:
		with open(Config.INFORMATION_FILE) as file:
			line = file.readline()
			essid = line.split("=")[1].strip('\n')
			line = file.readline()
			password = line.split("=")[1].strip('\n')

			return essid, password
	except IOError:
		return False


def save_information(essid: str, password: str):
	"""
	save information to file
	:param essid:
	:param password:
	:return: True if success else False
	"""
	from config import Config

	try:
		with open(Config.INFORMATION_FILE, "w") as file:
			file.write("essid=%s\n" % essid)
			file.write("password=%s" % password)
			file.flush()

		debug_info("information file created")
	except IOError:
		return False

	return True


def soft_reset():
	"""
	a soft reset
	"""

	from sys import exit

	exit()


def hard_reset():
	"""
	a hard reset
	"""

	from machine import reset

	reset()


def log(msg):
	debug_info(str(msg))

	filename = r"log.txt"

	with open(filename, "a+") as file:
		# now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
		file.write("[%s]: %s\n" % (__name__, str(msg)))
		file.flush()


def read_log():
	filename = r"log.txt"

	with open(filename, "r+") as file:
		print(file.read())
